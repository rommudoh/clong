(ns clong.main
  (:require [minicosm.core :refer [start!]]
            [minicosm.ddn :refer [render-to-canvas]]))

(enable-console-print!)

(defn init [] 
  {:game-state :ingame ;; :title / :ingame / :paused / :game-over
   :width 500
   :height 500
   :ball {:x 250 :y 250 :xspeed 0 :yspeed 0 :size 10}
   :paddles {:upper {:width 100 :height 20 :x 250}
             :lower {:width 100 :height 20 :x 250}}})

(defn assets [] 
  {})

(defn on-key [state key-evs]
  (cond-> state
    (key-evs "ArrowLeft") (update-in [:paddles :lower :x] - 3)
    (key-evs "ArrowRight") (update-in [:paddles :lower :x] + 3)
    (key-evs "KeyA") (update-in [:paddles :upper :x] - 3)
    (key-evs "KeyD") (update-in [:paddles :upper :x] + 3)))

(defn on-tick [state time]
  state)

(defn to-play [state assets is-playing] 
  {})

(defn draw-paddle [{:keys [x width height]} y]
  [:rect {:style :fill :pos [(- x (/ width 2)) (- y (/ height 2))] :dim [width height] :color "white"}])

(defn draw-ball [{:keys [x y size]}]
  [:circ {:style :fill :pos [x y] :r [size size] :color "red"}])

(defn to-draw [{:keys [paddles game-state width height ball] :as state} assets]
  (case game-state
    :ingame
    [:group {:desc "base"}
     [:rect {:style :fill :pos [0 0] :dim [width height] :color "black"}]
     (draw-paddle (:upper paddles) 0)
     (draw-paddle (:lower paddles) height)
     (draw-ball ball)]))

(start!
  {:init init
   :assets assets
   :on-key on-key
   :on-tick on-tick
   :to-play to-play
   :to-draw to-draw})
